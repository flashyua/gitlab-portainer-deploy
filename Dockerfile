FROM node:18-alpine

COPY . .

CMD ["node", "dist/index.js"]